#!/bin/bash
# run with sudo

# https://forums.raspberrypi.com/viewtopic.php?t=141052

apt update
apt upgrade -y

sed -i 's/#dtparam=spi=on/dtparam=spi=on/g' /boot/config.txt
echo 'dtoverlay=mcp2515-can0,oscillator=8000000,interrupt=12' >> /boot/config.txt
echo 'dtoverlay=spi-bcm2835-overlay' >> /boot/config.txt

apt install can-utils -y
reboot
